GIT

- install git on local computer
	https://git-scm.com/download/win
	use git from the windows prompt

GITLAB

- create gitlab project

- create ssh-key for accessing gitlab
	https://gitlab.webra.com/help/ssh/README
	c:\Program Files (x86)\Git\bin>ssh-keygen -t rsa -C "abc@webra.hu"
	enter location:  c:\Users\<currentuser>\.ssh\id_rsa
	
- add public key to gitlab
	https://gitlab.webra.com/profile/keys/new
	from c:\Users\<currentuser>\.ssh\id_rsa.pub
	
- install sourcetree
	https://www.sourcetreeapp.com/
	
- add server to known hosts
	ssh-keyscan -t rsa gitlab.com -> add result to c:\Users\<currentuser>\.ssh\known_hosts file

GITFLOW


	
SOURCETREE

- add gitlab project
	new/add working copy / 

valami demo
demo2
demo11 started